public class BlueFish extends Fish {
  
  public BlueFish() {
    super();
  }
  
  @Override
  protected void image() {
    canvas.image(fishFrames2[super.currImage],0, 0,super.sizeX,super.sizeY);
  }
}