import augmentaP5.*;
import TUIO.*;
import oscP5.*;
import ddf.minim.*;

AudioPlayer player;
Minim minim;
boolean mode3D = false;

HashMap<Integer, Octopus> persons = new HashMap<Integer, Octopus>();
int currentColor = 0;
PImage background;
ArrayList<Fish> fishs = new ArrayList<Fish>();
PImage[] fishFrames;
PImage[] fishFrames2;

void setup() {
  background(0);
  // /!\ Keep this setup order !
  setupSyphonSpout();
  setupAugmenta();
  setupGUI();
  background = loadImage("data/FondMarin.jpg");
  adjustSceneSize();
  // Add your code here
  fishFrames = new PImage[8];
  for(int i = 0; i < 8; i++) fishFrames[i] = loadImage("data/poisson/Poisson" + (i+1) + ".png");
  fishFrames2 = new PImage[8];
  for(int i = 0; i < 8; i++) fishFrames2[i] = loadImage("data/poisson2/Poisson" + (i+1) + ".png");
  for(int i = 0;i<8;i++) {
    fishs.add(new Fish());
  }
  for(int i = 8;i<16;i++) {
    fishs.add(new BlueFish());
  }
  minim = new Minim(this);
  player = minim.loadFile("data/Musique.mp3", 2048);
  player.loop();
}

void draw() {
  adjustSceneSize();
  background(0);
  
  // All visuals to send must be drawn in this canvas
  // Prefix your drawing functions with "canvas." as below
  canvas.beginDraw();
  canvas.background(0,50,200);
  canvas.image(background, 0,0,1280,1400);
  for(int i = 0;i<16;i++) {
    fishs.get(i).draw();
  }
  // Draw a blue disk for every persons
  AugmentaPerson[] people = auReceiver.getPeopleArray();
    for (int i=0; i<people.length; i++) {
    if(persons.get(people[i].pid) != null ) {
      canvas.pushMatrix();
        persons.get(people[i].pid).drawBubbles();
      canvas.popMatrix();
    }
  }
  for (int i=0; i<people.length; i++) {
    if(persons.get(people[i].pid) != null ) {
      canvas.pushMatrix();
        persons.get(people[i].pid).drawTentacles(people[i]);
      canvas.popMatrix();
    }
  }
  for (int i=0; i<people.length; i++) {
    if(persons.get(people[i].pid) != null ) {
      canvas.pushMatrix();
        persons.get(people[i].pid).drawBody(people[i]);
      canvas.popMatrix();
    }
  }
  
  drawAugmenta();

  canvas.endDraw();
  
  // Draw canvas in the window
  image(canvas, 0, 0, width, height);

  // Display instructions
  if(drawDebugData) {
    textSize(10);
    fill(255);
    text("Drag mouse to set the interactive area. Right click to reset.",10,height - 10);
  }
  
  // Syphon/Spout output
  sendFrames();
}

// You can also use these events functions which are triggered automatically

void personEntered (AugmentaPerson p) {
  println("Person entered : "+ p.pid + " at ("+p.centroid.x+","+p.centroid.y+")");
  persons.put(p.pid, new Octopus(OctoColor.fromNb(currentColor)));
  currentColor = (currentColor + 1)%3;
}

void personUpdated (AugmentaPerson p) {
  //println("Person updated : "+ p.pid + " at ("+p.centroid.x+","+p.centroid.y+")");
}

void personWillLeave (AugmentaPerson p) {
  persons.remove(p.pid);
  println("Person will leave : "+ p.pid + " at ("+p.centroid.x+","+p.centroid.y+")");
}

void stop()
{
  player.close();
  minim.stop();
  super.stop();
}