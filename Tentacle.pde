public class Tentacle {
  
  int numSegments = 12;
  float[] x = new float[numSegments];
  float[] y = new float[numSegments];
  float[] angle = new float[numSegments];
  float segLength = octoSize / 5.5;
  float targetX, targetY;
  
  int nbColor;
  OctoColor c;
  
  TentacleTarget target = new TentacleTarget();

  public Tentacle(OctoColor c) {
    this.c = c;
  }
  
  void draw(AugmentaPerson p) {
    target.randomMove();
    this.x[x.length-1] = p.centroid.x;
    this.y[x.length-1] = p.centroid.y;
    reachSegment(0, target.x, target.y);
    for(int i=1; i<numSegments; i++) {
      reachSegment(i, targetX, targetY);
    }
    for(int i=x.length-1; i>=1; i--) {
      positionSegment(i, i-1);  
    } 
    for(int i=0; i<x.length; i++) {
      segment(x[i], y[i], angle[i], (i+1)*(octoSize/40)); 
    }
  }
  
  void draw(AugmentaPerson p, PVector pos) {
    target.attack(pos);
    this.x[x.length-1] = p.centroid.x;
    this.y[x.length-1] = p.centroid.y;
    reachSegment(0, target.x, target.y);
    for(int i=1; i<numSegments; i++) {
      reachSegment(i, targetX, targetY);
    }
    for(int i=x.length-1; i>=1; i--) {
      positionSegment(i, i-1);  
    } 
    for(int i=0; i<x.length; i++) {
      segment(x[i], y[i], angle[i], (i+1)*(octoSize/40)); 
    }
  }

  void positionSegment(int a, int b) {
    x[b] = x[a] + cos(angle[a]) * segLength;
    y[b] = y[a] + sin(angle[a]) * segLength; 
  }
  
  void reachSegment(int i, float xin, float yin) {
    float dx = xin - x[i];
    float dy = yin - y[i];
    angle[i] = atan2(dy, dx);  
    targetX = xin - cos(angle[i]) * segLength;
    targetY = yin - sin(angle[i]) * segLength;
  }
  
  void segment(float x, float y, float a, float sw) {
    canvas.strokeWeight(sw);
    canvas.stroke(c.r(),c.g(),c.b(),100);
    canvas.pushMatrix();
    canvas.translate(x, y);
    canvas.rotate(a);
    canvas.line(0, 0, segLength, 0);
    canvas.popMatrix();
  }
}