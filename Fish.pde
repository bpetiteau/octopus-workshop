public class Fish {
  

  private int sizeX = 150;
  private int sizeY = 150;
  private float speedX;
  private float speedY;

  private PVector pos;
  private int currImage = 0;
  
  public Fish() {
    this.pos = new PVector(random(canvas.width), random(canvas.height));
    this.speedX = random(2)+0.3;
    this.speedY = random(2)+0.3;
  }
  
  public void draw() {
    canvas.pushMatrix();
      pos.x = pos.x + 2*speedX;
      pos.y = pos.y + 2*speedY;
      canvas.translate(pos.x-sizeX, pos.y-sizeY);
      if(pos.x > canvas.width) {
        pos.x = 0;
      }
      if(pos.y > canvas.height) {
        pos.y = 0;
      }
      if(frameCount == 3) currImage = (currImage +1)%8;
      frameCount = (frameCount +1)%4;
      image();
    canvas.popMatrix();
  }
  
  protected void image() {
    canvas.image(fishFrames[currImage],0, 0,sizeX,sizeY);
  }
}