public class TentacleTarget {

  float x = 0;
  float y = 0;
  int dirX = 1;
  int dirY = -1;
  
  int MOV_SPEED = 80;
  int ATT_SPEED = 60;
  
  public TentacleTarget() {
      this.x = random(octoSize*3)-octoSize*3/2;
      this.y = random(octoSize*3)-octoSize*3/2;
  }
  
  public void randomMove() {
    x = x + (random(MOV_SPEED)/10 * dirX);
    y = y + (random(MOV_SPEED)/10 * dirY);
    if(x > octoSize*3/2 || x < -octoSize*3/2) {
      dirX *= -1; 
    }
    if(y > octoSize*3/2 || y < - octoSize*3/2) {
      dirY *= -1; 
    }
  }
  
  public void attack(PVector pos) {
    float octoPosX = pos.x*canvas.width;
    float octoPosY = pos.y*canvas.height;
    if(this.x < octoPosX) this.x += random(ATT_SPEED)/10;
    else if (this.x > octoPosX) this.x -= random(ATT_SPEED)/10;
    if(this.y < octoPosY) this.y += random(ATT_SPEED)/10;
    else if(this.y > octoPosY) this.y -= random(ATT_SPEED)/10;
  }
}