import java.util.LinkedList;

int octoSize = 180; //173*203
int DIST_MIN = 30;
int BUBBLES = 6;

public class Octopus {

  private PImage image;
  private ArrayList<Tentacle> tentacles;
  private LinkedList<Bubble> bubbles;
  private Float lastAngle;
  private OctoColor c;
  private Octopus nearest;
  private PVector pos;
  private PVector lastPos;
  int frameCount = 0;
  
  public Octopus(OctoColor c) {
    this.c = c;
    this.image = loadImage(c.path());
    this.tentacles = new ArrayList<Tentacle>();
    for(int i = 0; i < 8; i++) {
      tentacles.add(new Tentacle(c));
    }
    this.bubbles = new LinkedList<Bubble>();
  }
  
  public void drawBubbles() {
    for(Bubble b : bubbles) {
      b.draw();
    }
  }
  
  public void drawTentacles(AugmentaPerson p) {
    pos = p.centroid; // Storing coordinates
    canvas.translate(pos.x*canvas.width, pos.y*canvas.height);    
    searchForNearest();
    if(nearest != null) {
      for(Tentacle t : tentacles) t.draw(p, new PVector(nearest.getPos().x-this.pos.x, nearest.getPos().y-this.pos.y));
    } else {
      for(Tentacle t : tentacles) t.draw(p);
    }
  }
  
  public void drawBody(AugmentaPerson p) {
    pos = p.centroid; // Storing coordinates
    if(frameCount == 59) {
      bubbles.add(new Bubble(c, pos));
      if(bubbles.size() > 6) {
        bubbles.poll();
      }
    }
    frameCount = (frameCount + 1)%60;
    canvas.translate(pos.x*canvas.width, pos.y*canvas.height);    
    //float angle = atan2(p.velocity.y, p.velocity.x) + PI/2;
    //canvas.rotate(smoothAngle(angle, p.centroid));
    canvas.image(image, -image.width/2, -image.height/2, image.width, image.height);
  }
  
  private float smoothAngle(float angle, PVector pos) {
    if(lastAngle == null) lastAngle = angle;
    float newAngle;
    if(lastPos != null && dist(pos.x*canvas.width, pos.y*canvas.width,lastPos.x*canvas.width, lastPos.y*canvas.height) > DIST_MIN) {
      newAngle = (lastAngle*0.65 + angle*0.35);
      lastAngle = newAngle;
    } else newAngle = lastAngle;
    lastPos = pos;
    return newAngle;
  }
  
  private void searchForNearest() {
    float minDist = 400;
    this.nearest = null;
    for(Octopus o : persons.values()) {
      if(o != null && o != this && pos != null && o.getPos() != null) {
        float tmpDist = dist(this.pos.x*canvas.width, this.pos.y*canvas.height, o.getPos().x*canvas.width, o.getPos().y*canvas.height);
        if (tmpDist <= minDist) {
          this.nearest = o;
          minDist = tmpDist;
        }
      }
    }
  }
  
  public PVector getPos() {
    return this.pos;
  }
}