public enum OctoColor {

  RED("data/Poulpe1.png", 0, 249,66,66),
  BLUE("data/Poulpe2.png", 1, 0,100,150),
  GREEN("data/Poulpe3.png", 2, 180,247,78);
  
  private String path;
  private int nbColor;
  private int r;
  private int g;
  private int b;
  
  private OctoColor(String path, int nbColor, int r, int g, int b) {
    this.path = path;
    this.nbColor = nbColor;
    this.r = r;
    this.g = g;
    this.b = b;
  }
  
  public static OctoColor fromNb(int nb) {
    switch(nb) {
      case 0:
        return RED;
      case 1:
        return BLUE;
      case 2:
        return GREEN;
      default:
        return RED;
    }
  }
  
  public String path() {
    return path;
  }
  
  public int nb() {
    return nbColor;
  }
  
  public int r() {
    return r;
  }
  
  public int g() {
    return g;
  }
  
  public int b() {
    return b;
  }
}