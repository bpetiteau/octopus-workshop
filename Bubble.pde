public class Bubble {

  private OctoColor c;
  private int size;
  private PVector pos;
  private int dirX;
  private int dirY;
  
  public Bubble(OctoColor c, PVector pos) {
    this.c = c;
    this.size = (int)random(30)+40;
    this.pos = new PVector(pos.x*canvas.width, pos.y*canvas.height);
    this.dirX = (int)random(2) == 0 ? -1 : 1;
    this.dirY = (int)random(2) == 0 ? -1 : 1;
  }
  
  public void draw() {
    canvas.pushMatrix();
      pos.x = pos.x + random(30)/10*dirX;
      pos.y = pos.y + random(30)/10*dirY;
      if(pos.x < 0 || pos.x > canvas.width) dirX *= -1;
      if(pos.y < 0 || pos.y > canvas.height) dirY *= -1;
      canvas.stroke(c.r(), c.g(), c.b(), 100);
      canvas.strokeWeight(10);
      canvas.fill(c.r(), c.g(), c.b(), 30);
      canvas.ellipse(pos.x, pos.y, size,size);
    canvas.popMatrix();
  }
}